FROM centos:7 

RUN yum install -y gcc libcurl-devel; \ 
    yum clean all; rm -rf /var/cache/yum

WORKDIR /usr/src
COPY restapi.c . 
RUN gcc -lcurl -o restapi restapi.c 
CMD /usr/src/restapi 
